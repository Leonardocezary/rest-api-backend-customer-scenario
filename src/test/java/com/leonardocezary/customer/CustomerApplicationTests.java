package com.leonardocezary.customer;

import com.leonardocezary.customer.model.db.customer.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CustomerApplicationTests {

    @Test
    void contextLoads() {
        Customer customer = Customer.builder()
                .customerId(1)
                .firstName("customerCreateRequest.getFirstName()")
                .lastName("customerCreateRequest.getLastName()")
                .city("customerCreateRequest.getCity()")
                .state("customerCreateRequest.getState()")
                .zip("customerCreateRequest.getZip()")
                .phoneNumber("customerCreateRequest.getPhoneNumber()")
                .sexType("customerCreateRequest.getSexType()")
                .age(21)
                .build();

        Customer customer2 = Customer.builder()
                .customerId(1)
                .firstName("customerCreateRequest.getFirstName()")
                .lastName("customerCreateRequest.getLastName()")
                .city("customerCreateRequest.getCity()")
                .state("customerCreateRequest.getState()")
                .zip("customerCreateRequest.getZip()")
                .phoneNumber("customerCreateRequest.getPhoneNumber()")
                .sexType("customerCreateRequest.getSexType()")
                .age(21)
                .build();

        if (customer.equals(customer2)){
            System.out.println("equals");
        }
    }

}
