package com.leonardocezary.customer.repository;

import com.leonardocezary.customer.model.db.customer.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> {

    @Query(value = " SELECT c FROM Customer c WHERE LOWER(c.firstName) LIKE LOWER(CONCAT('%', :name,'%')) OR LOWER(c.lastName) LIKE LOWER(CONCAT('%', :name,'%'))")
    Page<Customer> findAllByFirstNameOrLastName (@Param("name") String name, Pageable pageable);

    @Query(value = " SELECT c FROM Customer c WHERE " +
            "LOWER(c.firstName) LIKE LOWER(CONCAT('%', :filter,'%')) OR " +
            "LOWER(c.lastName) LIKE LOWER(CONCAT('%', :filter,'%')) OR " +
            "LOWER(c.city) LIKE LOWER(CONCAT('%', :filter,'%')) OR " +
            "LOWER(c.state) LIKE LOWER(CONCAT('%', :filter,'%')) OR " +
            "LOWER(c.zip) LIKE LOWER(CONCAT('%', :filter,'%')) OR " +
            "LOWER(c.phoneNumber) LIKE LOWER(CONCAT('%', :filter,'%')) OR " +
            "LOWER(c.sexType) LIKE LOWER(CONCAT('%', :filter,'%')) OR " +
            "LOWER(c.age) LIKE LOWER(CONCAT('%', :filter,'%'))")
    Page<Customer> findAllByFiltering(@Param("filter") String filter, Pageable pageable);
}
