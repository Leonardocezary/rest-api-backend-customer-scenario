package com.leonardocezary.customer.service;

import com.leonardocezary.customer.model.pojo.customer.CustomerPage;
import org.springframework.stereotype.Service;

@Service
public class CustomerPageServiceImpl implements CustomerPageService {

    @Override
    public CustomerPage createCustomerPage(Integer page, Integer size, String filterString, String sortProperty, Boolean sortAscending) {
        return CustomerPage.builder()
                .page(page)
                .size(size)
                .filterString(filterString)
                .sortProperty(sortProperty)
                .sortAscending(sortAscending)
                .build();
    }
}
