package com.leonardocezary.customer.service;

import com.leonardocezary.customer.model.db.customer.Customer;
import com.leonardocezary.customer.model.pojo.customer.CustomerPage;
import com.leonardocezary.customer.model.pojo.customer.CustomerSexType;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerCreateRequest;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerDeleteRequest;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerUpdateRequest;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerCreateResponse;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerDeleteResponse;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerUpdateResponse;
import com.leonardocezary.customer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.findById(customerId).get();
    }

    @Override
    public CustomerCreateResponse addCustomer(CustomerCreateRequest customerCreateRequest) {
        CustomerCreateResponse customerCreateResponse = null;

        try {
            customerCreateResponse = new CustomerCreateResponse();
            if (customerCreateRequest != null) {
                //use only our sex types
                if (Stream.of(CustomerSexType.values()).anyMatch(v -> v.getTypeName().equals(customerCreateRequest.getSexType()))) {
                    Customer customer = Customer.builder()
                            .firstName(customerCreateRequest.getFirstName())
                            .lastName(customerCreateRequest.getLastName())
                            .city(customerCreateRequest.getCity())
                            .state(customerCreateRequest.getState())
                            .zip(customerCreateRequest.getZip())
                            .phoneNumber(customerCreateRequest.getPhoneNumber())
                            .sexType(customerCreateRequest.getSexType())
                            .age(customerCreateRequest.getAge())
                            .build();

                    customerRepository.save(customer);
                    customerCreateResponse.setCreateSucceeded(true);
                }
            } else {
                customerCreateResponse.setErrorMessage("The create user request object is NULL");
                //todo throw custom exception
            }
        }
        //todo catch custom exception
        catch (Exception e) {
            customerCreateResponse.setCreateSucceeded(false);
            customerCreateResponse.setErrorMessage(String.format("There's been an error during create: %s", e.getMessage()));
        }

        return customerCreateResponse;
    }

    @Override
    public CustomerUpdateResponse updateCustomer(CustomerUpdateRequest customerUpdateRequest) {
        CustomerUpdateResponse customerUpdateResponse = null;
        try {
            customerUpdateResponse = new CustomerUpdateResponse();
            if (customerUpdateRequest != null && customerUpdateRequest.getCustomerId() != null){
                Integer customerId = customerUpdateRequest.getCustomerId();
                Customer currentCustomer = getCustomerById(customerId);

                if (currentCustomer != null) {
                    Customer updatedCustomer = Customer.builder()
                            .customerId(customerId)
                            .firstName(customerUpdateRequest.getFirstName())
                            .lastName(customerUpdateRequest.getLastName())
                            .city(customerUpdateRequest.getCity())
                            .state(customerUpdateRequest.getState())
                            .zip(customerUpdateRequest.getZip())
                            .phoneNumber(customerUpdateRequest.getPhoneNumber())
                            .sexType(customerUpdateRequest.getSexType())
                            .age(customerUpdateRequest.getAge())
                            .build();

                    customerRepository.save(updatedCustomer);
                    customerUpdateResponse.setUpdateSucceeded(true);
                }
            }
            else {
                customerUpdateResponse.setErrorMessage("The update user request object is NULL");
                //todo throw custom exception
            }
        }
        catch (Exception e){
            customerUpdateResponse.setUpdateSucceeded(false);
            customerUpdateResponse.setErrorMessage(String.format("There's been an error during update: %s", e.getMessage()));
        }

        return customerUpdateResponse;
    }

    @Override
    public CustomerDeleteResponse deleteCustomer(CustomerDeleteRequest customerDeleteRequest) {
        CustomerDeleteResponse customerDeleteResponse = null;

        try {
            customerDeleteResponse = new CustomerDeleteResponse();
            if (customerDeleteRequest != null) {
                customerRepository.deleteById(customerDeleteRequest.getCustomerId());
                customerDeleteResponse.setDeleteSucceeded(true);
            } else {
                customerDeleteResponse.setErrorMessage("The delete user request object is NULL");
                //todo throw custom exception
            }
        }
        //todo catch custom exception
        catch (Exception e){
            customerDeleteResponse.setDeleteSucceeded(false);
            customerDeleteResponse.setErrorMessage(String.format("There's been an error during delete: %s", e.getMessage()));
        }

        return customerDeleteResponse;
    }

    @Override
    public Page<Customer> getPageableCustomers(CustomerPage customerPage) {

        Page<Customer> paginatedCustomers = null;
        try {
            if (customerPage != null) {
                Sort sort = null;

                //check for sort
                if (customerPage.getSortProperty() != null && customerPage.getSortAscending() != null){
                    Sort.Direction sortDirection = customerPage.getSortAscending() ? Sort.Direction.ASC: Sort.Direction.DESC;
                    sort = Sort.by(sortDirection, customerPage.getSortProperty());
                }

                //create the pageable with sorting or not
                Pageable pageable = (sort != null)? PageRequest.of(customerPage.getPage(), customerPage.getSize(), sort): PageRequest.of(customerPage.getPage(), customerPage.getSize());

                //check for filtering and retrieve the paginated customers
                if (customerPage.getFilterString() != null){
                    paginatedCustomers = customerRepository.findAllByFirstNameOrLastName(customerPage.getFilterString(), pageable);
                }
                else {
                    paginatedCustomers = customerRepository.findAll(pageable);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return paginatedCustomers;
    }
}
