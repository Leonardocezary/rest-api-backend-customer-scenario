package com.leonardocezary.customer.service;

import com.leonardocezary.customer.model.pojo.customer.CustomerPage;

public interface CustomerPageService {
    
    CustomerPage createCustomerPage(Integer page, Integer size, String filterString, String sortProperty, Boolean sortAscending);
}
