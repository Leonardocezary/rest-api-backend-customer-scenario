package com.leonardocezary.customer.service;

import com.leonardocezary.customer.model.db.customer.Customer;
import com.leonardocezary.customer.model.pojo.customer.CustomerPage;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerCreateRequest;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerDeleteRequest;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerUpdateRequest;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerCreateResponse;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerDeleteResponse;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerUpdateResponse;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CustomerService {

    Customer getCustomerById(Integer customerId);
    CustomerCreateResponse addCustomer(CustomerCreateRequest CustomerCreateRequest);
    CustomerUpdateResponse updateCustomer(CustomerUpdateRequest customerUpdateRequest);
    CustomerDeleteResponse deleteCustomer(CustomerDeleteRequest customerDeleteRequest);
    Page<Customer> getPageableCustomers(CustomerPage customerPage);
}
