package com.leonardocezary.customer.controller.customer;

import com.leonardocezary.customer.model.db.customer.Customer;
import com.leonardocezary.customer.model.pojo.customer.CustomerPage;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerCreateRequest;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerDeleteRequest;
import com.leonardocezary.customer.model.pojo.customer.request.CustomerUpdateRequest;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerCreateResponse;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerDeleteResponse;
import com.leonardocezary.customer.model.pojo.customer.response.CustomerUpdateResponse;
import com.leonardocezary.customer.service.CustomerPageService;
import com.leonardocezary.customer.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private Logger logger;
    private CustomerService customerService;
    private CustomerPageService customerPageService;

    @Autowired
    public CustomerController(CustomerService customerService, CustomerPageService customerPageService){
        this.customerService = customerService;
        this.customerPageService = customerPageService;
        logger = LoggerFactory.getLogger(CustomerService.class);
    }

    @GetMapping(path = "/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Customer> getCustomerById(@PathVariable("customerId") Integer customerId) {
        logger.debug("Entering getCustomerById method");
        return ResponseEntity.ok(customerService.getCustomerById(customerId));
    }

    @PostMapping(path = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<CustomerCreateResponse> createCustomer(@RequestBody CustomerCreateRequest customerCreateRequest) {
        logger.debug("Entering createCustomer method");
        return ResponseEntity.ok(customerService.addCustomer(customerCreateRequest));
    }

    @PutMapping(path = "/update", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CustomerUpdateResponse updateCustomer(@RequestBody CustomerUpdateRequest customerUpdateRequest) {
        logger.debug("Entering updateCustomer method");
        return customerService.updateCustomer(customerUpdateRequest);
    }

    @DeleteMapping(path = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<CustomerDeleteResponse> deleteCustomer(@RequestBody CustomerDeleteRequest customerDeleteRequest) {
        logger.debug("Entering deleteCustomer method");
        return ResponseEntity.ok(customerService.deleteCustomer(customerDeleteRequest));
    }

    @GetMapping("/customers")
    @ResponseBody
    public ResponseEntity<Page<Customer>> getPageableCustomers(@Param(value = "page") Integer page,
                                                               @Param(value = "size") Integer size,
                                                               @Param(value = "filterString") String filterString,
                                                               @Param(value = "sortProperty") String sortProperty,
                                                               @Param(value = "sortAscending") Boolean sortAscending) {
        logger.debug("Entering getList method");
        CustomerPage customerPage = customerPageService.createCustomerPage(page, size, filterString, sortProperty, sortAscending);
        return ResponseEntity.ok(customerService.getPageableCustomers(customerPage));
    }
}
