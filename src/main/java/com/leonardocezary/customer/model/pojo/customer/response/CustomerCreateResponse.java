package com.leonardocezary.customer.model.pojo.customer.response;

import lombok.*;

@Data
@NoArgsConstructor
public class CustomerCreateResponse {

    private boolean createSucceeded;
    private String errorMessage;
}
