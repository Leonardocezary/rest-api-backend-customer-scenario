package com.leonardocezary.customer.model.pojo.customer.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class CustomerDeleteRequest {

    private Integer customerId;
}
