package com.leonardocezary.customer.model.pojo.customer.response;

import lombok.*;

@Data
@NoArgsConstructor
public class CustomerUpdateResponse {

    private boolean updateSucceeded;
    private String errorMessage;
}
