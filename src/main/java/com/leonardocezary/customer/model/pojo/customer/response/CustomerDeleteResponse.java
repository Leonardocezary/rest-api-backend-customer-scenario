package com.leonardocezary.customer.model.pojo.customer.response;

import lombok.*;
import lombok.experimental.Accessors;

//@Accessors(fluent = true)
@Data
@NoArgsConstructor
public class CustomerDeleteResponse {

    private boolean deleteSucceeded;
    private String errorMessage;
}
