package com.leonardocezary.customer.model.pojo.customer;

public enum CustomerSexType {
    MALE("Male"),
    FEMALE("Female"),
    OTHER("Other");

    private String typeName;

    CustomerSexType(String typeName){
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    /**
     *
     * @param typeName
     * @return
     */
    public static CustomerSexType fromStatusName(String typeName) {
        for (CustomerSexType customerSexType : values()) {
            if (customerSexType.getTypeName().equals(typeName)) {
                return customerSexType;
            }
        }
        return null;
    }
}
