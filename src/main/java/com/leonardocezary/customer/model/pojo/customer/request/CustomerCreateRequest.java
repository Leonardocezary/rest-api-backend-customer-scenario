package com.leonardocezary.customer.model.pojo.customer.request;

import com.leonardocezary.customer.model.pojo.customer.CustomerSexType;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class CustomerCreateRequest {

    private String firstName;
    private String lastName;
    private String city;
    private String state;
    private String zip;
    private String phoneNumber;
    private String sexType;
//    private CustomerSexType sexType;
    private Integer age;
}
