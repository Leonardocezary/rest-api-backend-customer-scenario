package com.leonardocezary.customer.model.pojo.customer;

import lombok.*;

@Builder
@Getter
@EqualsAndHashCode
@ToString
public class CustomerPage {

    private Integer page;
    private Integer size;
    private String filterString;
    private String sortProperty;
    private Boolean sortAscending;
}
