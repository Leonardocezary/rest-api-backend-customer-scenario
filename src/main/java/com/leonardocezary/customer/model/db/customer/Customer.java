package com.leonardocezary.customer.model.db.customer;

import com.leonardocezary.customer.model.pojo.customer.CustomerSexType;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "customer")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer {

    @Id
    @Column(name = "customer_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer customerId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zip")
    private String zip;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "sex")
//    @Enumerated(EnumType.STRING)
    private String sexType;

    @Column(name = "age")
    private Integer age;
}
