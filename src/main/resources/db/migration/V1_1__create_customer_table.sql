CREATE TABLE IF NOT EXISTS `customer` (
    `customer_id` int(11) NOT NULL AUTO_INCREMENT,
    `first_name` varchar(50) NOT NULL,
    `last_name` varchar(50) NOT NULL,
    `city` varchar(50) NOT NULL,
    `state` varchar(50) NOT NULL,
    `zip` varchar(50) NOT NULL,
    `phone_number` varchar(50) NOT NULL,
    `sex` varchar(50) NOT NULL,
    `age` int(11) NOT NULL,
    PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
