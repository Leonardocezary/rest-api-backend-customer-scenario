# REST API backend Customer scenario

This project is  a REST API backend  which implements basic CRUD operations on a Customer scenario.
It also does server side pagination with sorting and filtering.
It's using Java 8 and Spring Boot 2.3.4, MySQL5.7, JPA, Hibernate and Flyway

# How to run it
* Make sure you're running Java 8 and MySQL 5.7
* Create a database called **customers**, an user called **customer** with password set as **password**
* Grant the customer user the rights for the customers table.
* Set the UTC timezone for the database if needed
* Run maven install
* Execute flyway db.migration files
* Start the application as a Spring application (has Tomcat embedded)

## Swagger UI
* This application has implemented Swagger so you can test the APIs
* Open your browser and go to http://localhost:8080/swagger-ui/index.html

